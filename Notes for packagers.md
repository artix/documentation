### Notes for maintainers
-----

The main tools used for packaging are `artools`, `artix-checkupdates`, and `artix-rebuild-order`. These are all provided by the `artix-tools` group. `artools-pkg` comes with `artixpkg` which is the main program for pushing package updates. `artix-checkupdates` is what compares current artix package versions to upstream arch package versions and determines if a package update or package move is needed.

-----
Here's a quick overview of `artixpkg`:
~~~
% artixpkg -h
    Usage: artixpkg [COMMAND] [OPTIONS]

    COMMANDS
        repo      Pacman database modification for package updates, moves etc
        git       Manage Git packaging repositories and their configuration
        admin     Management of topics and obsolete repos
        version   Show artixpkg version information

    OPTIONS
        -h, --help     Show this help text
~~~

Usage is mostly self explanatory. Every command has additional subcommands where you can pass the `-h` argument for more help output.

Examples:
~~~
1. artixpkg git clone foo # clone package foo to your artools-workspace directory
2. artixpkg git create -c bar # create new package, bar, and clone it to your artools-workspace directory
3. artixpkg repo add -p world foo # add package foo to world
4. artixpkg repo move -p world-gremlins world foo # move package foo from world-gremlins to world
5. artixpkg repo add -r -p bar world-goblins # add package bar to world-goblins and mark it as a rebuild
~~~

`artixpkg` is capable of accepting multiple packages in its arguments making it easily scriptable.

#### Additional notes:

All packages in **_system_** **must** go through **[system-gremlins]** first! NO EXCEPTIONS!

When putting packages in galaxy, you **must** set the agent name on the repo **before** pushing. e.g.: `artixpkg git config -a galaxy foo`.


To compare Artix vs Arch, you will want to use artix-checkupdates:
~~~
% artix-checkupdates -h
Usage: artix-checkupdates [options]
-a,--artix            Show packages that are only in artix.
-b,--blacklist        Repos to skip when listing upgrades (comma-delineated).
-d,--downgrades       Show package downgrades.
-f,--filter           Filter output on packages present in artix tree directory.
-h,--help             Show this help message.
-l,--loose-moves      Consider higher versions in Arch as a package move.
-m,--moves            Show package moves.
-n,--no-sync          Don't sync package databases.
-u,--upgrades         Show package upgrades.
-w,--write <file>     Write artixpkg add/move commands in a bash script.
~~~

This has several options, but in general `artix-checkupdates` pretty prints a table in the terminal.

Examples:
~~~
1. artix-checkupdates -mu # show package upgrades and package moves
2. artix-checkupdates -m -w out.sh # show package moves and write the artixpkg move commands to a shell script
3. artix-checkupdates -mu -b system # show package upgrades and package moves but skip the system repo
~~~
