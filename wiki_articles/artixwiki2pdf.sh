#!/bin/sh

# /usr/local/bin/artixwiki2pdf.sh
# Dependencies:
# pdf-crop-margins: pip install pdfCropMargins
# wkhtmltopdf
# pdfsizeopt-git

#https://wiki.artixlinux.org/Main/Migration
#https://wiki.artixlinux.org/Main/Installation
#https://wiki.artixlinux.org/Main/Configuration
#https://wiki.artixlinux.org/Main/Troubleshooting
#https://wiki.artixlinux.org/Main/OpenRC
#https://wiki.artixlinux.org/Main/Runit
#https://wiki.artixlinux.org/Main/S6

startdir=`pwd`

URL="https://wiki.artixlinux.org/Main/"
ARTICLES=("Migration Installation Configuration Troubleshooting OpenRC Runit S6 Dinit")
ACTION="?action=print"

function strip_css_elements {
cat >|local.css <<EOF
p { word-break: normal; }
a { color: #80d3fc; text-decoration: none; }
small { display: none !important; }
.mini { padding:2px; border:1px solid #c6c6c6; background-color:#1a1a1a; }
.miniH, .miniF { display:block; }
a:visited { color: #cc77cc; }
code, pre { color: #99aabb; word-break: normal !important; }
#wikicmds { display: none; }
wikitrail { background-color: #232423; }
.container-fluid { padding-right: 2px; padding-left: 2px; }
.row-fluid .span9 { width: 98%; }
.well { display: none; }
EOF
}

mkdir -p temp Finished
cd temp
for article in ${ARTICLES[@]}; do
    rm -f *.css
    link=${URL}${article}
    echo -n "Fetching $link... "
    wget -q -p -k -nd $link && echo "success."
    mv ${article} ${article}.html
    strip_css_elements
    echo -n "  Converting $article.html to pdf... "
    wkhtmltopdf -q -s a5 ${article}.html ${article}.pdf && echo "success."
    echo -n "  Cropping $article.pdf borders... "
    pdf-crop-margins -p 0 ${article}.pdf -o ${article}-crop.pdf && echo "success."
    echo -n "  Size-optimizing $article.pdf... "
    pdfsizeopt --do-require-image-optimizers=no --v=0 ${article}-crop.pdf ../Finished/${article}.pdf && echo "success."
done
cd $startdir
rm -fr temp
mv Finished/*.pdf .
rm -fr Finished
